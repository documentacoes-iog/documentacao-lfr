﻿# Leituras de Qualidade

O _IoG Integration_ recebe solicitações de envio de Leituras de Qualidade mensal do SAP. No Deadline fornecido pelo SAP o _IoG Integration_ solicita ao MDC IoG esses parâmetros e por isso essas leituras devem estar na base de dados do _IoG Services_.

Para inserir novas leituras de qualidade o usuário deve enviar para o _Middleware_ uma lista de leituras de qualidade. O _Middleware_ irá verificar, processar e encaminhar para o _IoG Services_ via MQTT para que possa ser salvo no banco de dados.
Leituras inseridas não podem ser apagadas via API.



## Inserção de Leituras de Qualidade

**Sistema** Middleware outros MDCs |
**URL** | `/quality-readings/{serial}`
**Method** | `POST`
**Permissions required** | usuário ADMIN ou INTEGRATION autenticado

Os campos do objeto de leitura de qualidade são descritos na [tabela - objeto leitura de qualidade](). O endpoint recebe uma lista desse objeto.

#### Leitura de Qualidade - Descrição de objeto

| nome         | tipo      | obrigatório | tamanho | descrição                         |
|-----|-----------|-------------|---------|-----------------------------------|
|  DateTimeInitial   | datetime* | não         | -       | Data e Hora do início do conjunto |
|  DateTimeFinal   | datetime* | sim         | -       | Data e Hora do final do conjunto  |
|  DRP   | float     | sim         | 1-3     | Parâmetro DRP                     |
|  DRC   | float     | sim         | 1-3     | Parâmetro DRC                     |
|  DTT95  | float     | não         | -       | Parâmetro DTT95                   |
|FD95| float     | não         | -       | Parâmetro FD95                   |

*String datetime no formato yyyy-MM-ddTHH:mm:ssZ

### Exemplo de Inserção de leituras de qualidade

```json
[
  {
    "DateTimeInitial": "2022-04-10T22:02:33.247Z",
    "DateTimeFinal": "2022-04-10T22:02:33.247Z",
    "DRP": 5.1,
    "DRC": 1
  },
  {
    "DateTimeInitial": "2022-03-10T22:02:33.247Z",
    "DateTimeFinal": "2022-03-10T22:02:33.247Z",
    "DRP": 99.4,
    "DRC": 56
  }
]
```
### Success Response

**Code** : `200 OK`
