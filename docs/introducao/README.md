﻿# Introdução

O módulo LFR é responsavél pela configuração dos dispositivos e pelo e agendamento de processos de update de firmware, assim como a visualização dos dispositivos e suas respetivas versões de firmwares.

As funcionalidas solicitadas pelo LFR são:

- **CRUD DE DISPOSITIVOS**
- **VISUALIZAÇÕES NO MAPA DOS DISPOSITIVOS**
- **CRUD DE VERSÕES DE FIRMWARE**
- **AGENDAMENTOS DE UPDATE DE FIRMWARE**
- **RELATORIOS DE UM UPDATE**
- **VISUALIZAÇÕES DE UPDATES**



# Caso de Uso de Sistema
![](./LFR.png)

