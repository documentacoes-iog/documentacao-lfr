﻿# CRUD

Devido à limitações de recurso para subir novas instâncias do SAP, não é possível utilizar o _Integration IoG_ para **cadastrar** medidores de MDCs que não sejam o _IoG_ através do SAP.

Por esse motivo, o cadastro dos medidores deve ser realizado primeiramente no MDC de destino e este realizar o processo de cadastro no IoG.

O IoG Integration cadastra um novo medidor quando o SAP envia uma solicitação de cadastro e o _Serial_ do medidor enviado não se encontra na base de dados do _IoG Services_. Quando o medidor existe na base, o IoG Integration realiza a edição do medidor sem alterar a _Company_.

**Caso o medidor não exista, o _IoG Integration_ irá criar um medidor associado a NIC NEPEN OBRIGATORIAMENTE. Por esse motivo o cadastro de medidores de outros MDCs deve ser realizado antes da instalação e envio pelo SAP**.

## Cadastro de um medidor

**Sistema** IoG Services |
**URL** | `/meters`
**Method** | `POST`
**Permissions required** | usuário ADMIN ou INTEGRATION autenticado

Para cadastro de um novo medidor são obrigatórios os campos descritos na tabela [Meter - Descrição de objeto](). Os demais campos do objeto do medidor serão criados/atualizados quando o SAP enviar a solicitação de cadastro para o medidor.

#### Meter - Descrição de objeto
| nome         | tipo           | obrigatório | tamanho | descrição                                                                          | 
|--------------|----------------|-------------|---------|------------------------------------------------------------------------------------|
| Serial       | String         | Sim         | 1-18    | Identificador único do medidor                                                     |
| Installation | String         | Sim         | 1-10    | Identificador único do cliente                                                     |
| Phase        | Enum           | Sim         | 1       | Quantidade de fases do medidor                                                     |
| MeterModel   | [MeterModel]() | Sim         | -       | Objeto que identifica o modelo do medidor. [Meter Model - Descrição de objeto ]( ) |
| RateType     | [RateType]()   | Sim         | -       | Objeto que identifica o tipo de tarifa. [RateType - Descrição de objeto]( )        |
| Company*     | Enum           | Sim         | 1       | Identificador da empresa responsável pela NIC que está no medidor                  |


#### Meter Model - Descrição de objeto 
| nome         | tipo             | obrigatório | tamanho | descrição                                                  |
|--------------|------------------|-------------|---------|------------------------------------------------------------|
| Name         | String           | Sim         | 1-20    | Nome do Modelo                                             |
| Manufacturer | [Manufacturer]() | Sim         | -       | Objeto que identifica o fabricante do medidor [Manufacturer - Descrição de objeto]() |

#### Manufacturer - Descrição de objeto

** O cadastro de _Manufacturer_ é feito pela equipe NEPEN. Caso seja necessário adicionar novos, entrar em contato conosco.

| nome         | tipo   | obrigatório | tamanho | descrição                                                                                               |
|--------------|--------|-------------|---------|---------------------------------------------------------------------------------------------------------|
| Name         | String | Sim         | 1-20    | Nome do Fabricante Fabricantes aceitos**: <br/>* Eletra <br/>* Nansen <br/>* Star Measure <br/>* Waison |



#### RateType - Descrição de objeto
| nome         | tipo   | obrigatório | tamanho | descrição                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|--------------|--------|-------------|---------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Name         | String | Sim         | 1-20    | Nome da Tarifa <br/>Tarifas aceitas: <br/>* A1_AZ <br/>* A1_CONV <br/>* A1_LVAZ <br/>* A1_LVVD <br/>* A1_VD <br/>* A2_AZ <br/>* A2_CONV <br/>* A2_GER <br/>* A2_GERAZ <br/>* A2_GERVD <br/>* A2_LVAZ <br/>* A2_LVVD <br/>* A2_VD <br/>* A3_AZ <br/>* A3_CONV <br/>* A3_GER <br/>* A3_GERAZ <br/>* A3_GERVD <br/>* A3_LVAZ <br/>* A3_LVVD <br/>* A3_VD <br/>* A3A_AZ <br/>* A3A_CONV <br/>* A3A_GER <br/>* A3A_GERAZ <br/>* A3A_GERVD <br/>* A3A_LVAZ <br/>* A3A_LVVD <br/>* A3A_VD <br/>* A3_GERVD <br/>* A4_AZ <br/>* A4_CONV <br/>* A4_GER <br/>* A4_GERAZ <br/>* A4_GERVD <br/>* A4_LVAZ <br/>* A4_LVVD <br/>* A4_VD <br/>* B1_BRANCA <br/>* B1_RESID <br/>* B2_BRANCA <br/>* B2_RUR <br/>* B3_BRANCA <br/>* B3_OUTROS <br/>* B4A_IP <br/>* B4B_IP <br/>* DUMMY <br/>* MED_FISCAL <br/>* MIGRADO |

#### Company - Valores

*Este campo é atualizado conforme novos MDCs integram com o _IoG_. Deve ser solicitado à equipe NEPEN o valor para a nova empresa que deseja integrar.

| nome  | valor  |
|-------|--------|
| Nepen | 0      |
| Others| 99999  |


### Exemplo de cadastro (somente campos obrigatórios)
```json
{
  "Serial": "1234567899876543210",
  "Installation": "123456789987654321",
  "Phase": 1,
  "MeterModel": {
    "Name": "zeus8121",
    "Manufacturer": {
      "Name": "eletra"
    }
  },
  "RateType": {
    "Name": "b1_resid"
  },
  "Company": 0
}
```

### Success Response

**Code** : `200 OK`

## Edição de um medidor

O medidor será editado quando o SAP enviar a solicitação de cadastro. Os campos que o SAP envia e que serão editados (exceto serial, phase e company) são descritos na tabela []().

#### Descrição de objetos enviados pelo SAP
| nome            | tipo           | tamanho | descrição                                                                          | 
|-----------------|----------------|---------|------------------------------------------------------------------------------------|
| Serial          | String         | 1-18    | Identificador único do medidor. Não editável.                                      | 
| Installation    | String         | 1-10    | Identificador único do cliente                                                     | 
| Phase           | Enum           | 1       | Quantidade de fases do medidor. Não editável.                                      |
| MeterModel      | [MeterModel]() | -       | Objeto que identifica o modelo do medidor. [Meter Model - Descrição de objeto ]( ) | 
| RateType        | [RateType]()   | -       | Objeto que identifica o tipo de tarifa. [RateType - Descrição de objeto]( )        |
| Company*        | Enum           | 1       | Identificador da empresa responsável pela NIC que está no medidor. Não editável    | 
| Identifier      | Enum           | 1       | "I" - Instalado, "R" - Retirado                                                    | 
| ConnectionPhase | Enum           | 1       | Fase de Ligação                                                                    |    
| MeterRegisters  | MeterRegisters | 1       | Código do registrador habilitado no equipamento                                    |  
| Latitude  | String         | -       | Latitude onde o equipamento está instalado                                         |  
| Longitude  | String         | -       | Longitude onde o equipamento está instalado                                                                                   |  

## Deleção de um medidor

O SAP não solicita deleção de medidores no MDC. Caso o MDC que esteja integrando deseje remover algum medidor da base de dados, a operação pode ser realizada via API.

### Exemplo de deleção

**Sistema** IoG Services |
**URL** | `/meters/{serial}`
**Method** | `DELETE`
**Permissions required** | usuário ADMIN ou INTEGRATION autenticado

### Success Response

**Code** : `200 OK`

