﻿# Leituras 

O _IoG Integration_ recebe solicitações de envio de Leituras do SAP. No Deadline fornecido pelo SAP o _IoG Integration_ solicita ao _MDC IoG_ a leitura de um dia específico. Essa leitura (que depende da categoria da tarifa) deve se encontrar na base do _IoG_ e ela pode ser retornada por dados de Memória de Massa ou Frozen Data.

Para inserir novas leituras o usuário deve enviar para o _Middleware_ uma lista de Memória de Massas e/ou Frozen Datas. O Middleware irá verificar, processar e encaminhar para o _IoG Services_ via MQTT para que possa ser salvo no banco de dados.
Leituras inseridas não podem ser apagadas via API.

## Inserção de Memória de Massa

**Sistema** Middleware outros MDCs |
**URL** | `/mass-memory/{serial}`
**Method** | `POST`
**Permissions required** | usuário ADMIN ou INTEGRATION autenticado

Os campos do objeto de Memória de Massa são descritos na [tabela - objeto Memória de Massa](). O endpoint recebe uma lista desse objeto.

Todos os campos - **exceto _Datetime_ e _TotalEnergy_Wh_** - são nullables.
#### Memória de Massa - Descrição de objeto

| nome         | tipo      | obrigatório | tamanho | descrição                                 |
|-----|-----------|-------------|---------|-------------------------------------------|
|  DateTime   | datetime* | sim         | -       | Data e Hora da leitura                    |
|  AmrProfileStatus   | datetime* | não         | -       | Amr Profile Status                        |
|  TotalEnergy_Wh   | double    | sim         | 1-3     | Energia Ativa Direta em Wh                |
|  TotalDirectActiveEnergy_Wh   | double    | não         | 1-3     | Energia Ativa Direta Total em Wh          |
|  BorderTotalizer_Wh  | double    | não         | -       | Energia Ativa Direta T1 em Wh             |
|ReservedTotalizer_Wh| double     | não         | -       | Energia Ativa Direta T2 em  Wh            |
|OffBorderTotalizer_Wh| double     | não         | -       | Energia Ativa Direta T3 em Wh             |
|TotalIntermediary_Wh| double     | não         | -       | Energia Ativa Direta T4 em  Wh            |
|GeneratedEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa em  Wh              |
|TotalReverseActiveEnergy_Wh| double     | não         | -       | Energia Ativa Reversa Total em Wh         |
|GeneratedOffBorderEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T1 em Wh            |
|GeneratedIntermediaryEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T2 em Wh            |
|GeneratedBorderEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T3 em Wh            |
|GeneratedReservedEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T4 em Wh            |
|Q1ReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Q1 em VarH                |
|Q2ReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Q2 em VarH                |
|Q3ReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Q3 em VarH                |
|Q4ReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Q4  em VarH               |
|ReactiveEnergyImportQiRi_VarH| double     | não         | -       | Import Energia Ativa QI (+Ri) em VarH     |
|ReactiveEnergyImportQivRc_VarH| double     | não         | -       | Import Energia Ativa QIV (-Rc) em VarH    |
|PhaseAVoltageAverage_V| double     | não         | -       | Tensão Média Fase A em V                  |
|PhaseBVoltageAverage_V| double     | não         | -       | Tensão Média Fase B em V                  |
|PhaseCVoltageAverage_V| double     | não         | -       | Tensão Média Fase C em V                  |
|LastAverageVoltageL1_V| double     | não         | -       | Última média de tensão L1 em V            |
|LastAverageVoltageL2_V| double     | não         | -       | Última média de tensão L2  em  V          |
|LastAverageVoltageL3_V| double     | não         | -       | Última média de tensão L3  em V           |
|PhaseACurrentAverage_A| double     | não         | -       | Corrente Média Fase A em A                |
|PhaseBCurrentAverage_A| double     | não         | -       | Corrente Média Fase B em A                |
|PhaseCCurrentAverage_A| double     | não         | -       | Corrente Média Fase C em A                |
|LastAverageCurrentL1_A| double     | não         | -       | Última média de Corrente L1 em A          |
|LastAverageCurrentL2_A| double     | não         | -       | Última média de Corrente L2 em A          |
|LastAverageCurrentL3_A| double     | não         | -       | Última média de Corrente L3 em A          |
|TotalPowerFactor_NoUnit| double     | não         | -       | Fator de Potência Total                   |
|PhaseAVoltageThd_NoUnit| double     | não         | -       | THD Tensão Fase A                         |
|PhaseBVoltageThd_NoUnit| double     | não         | -       | THD Tensão Fase B                         |
|PhaseCVoltageThd_NoUnit| double     | não         | -       | THD Tensão Fase C                         |
|PhaseACurrentThd_NoUnit| double     | não         | -       | THD Corrente Fase A                       |
|PhaseBCurrentThd_NoUnit| double     | não         | -       | THD Corrente Fase B                       |
|PhaseCCurrentThd_NoUnit| double     | não         | -       | THD Corrente Fase C                       |
|Group0DirectEnergy_Wh| double     | não         | -       | Grupo 0 Energia Direta em Wh              |
|Group0InductiveReactiveDirectEnergy_VArhPlus| double     | não         | -       | Grupo 0 Energia Direta em WArh+           |
|Group0CapacitiveReactiveDirectEnergy_VArhMinus| double     | não         | -       | Grupo 0 Energia Direta em WArh-           |
|Group1ReverseEnergy_Wh| double     | não         | -       | Grupo 1 Energia Reversa em Wh             |
|Group1InductiveReactiveReverseEnergy_VArhPlus| double     | não         | -       | Grupo 1 Energia Reversa em  WArh+         |
|Group1CapacitiveReactiveReverseEnergy_VArhMinus| double     | não         | -       |                                           |
|Group2VoltageVaH_V| double     | não         | -       | Grupo 2 Tensão Va_h em V                  |
|Group2VoltageVbH_V| double     | não         | -       | Grupo 2 Tensão Vb_h em V                  |
|Group2VoltageVcH_V| double     | não         | -       | Grupo 2 Tensão Vc_h em V                  |
|Group3CurrentIaH_A| double     | não         | -       | Grupo 3 Corrente Ia_h em A                |
|Group3CurrentIbH_A| double     | não         | -       | Grupo 3 Corrente Ib_h em A                |
|Group3CurrentIcH_A| double     | não         | -       | Grupo 3 Corrente Ic_h em A                |
|Group4ThdVoltageVaH_NoUnit| double     | não         | -       | Grupo 4 Tensão THD Va_h                   |
|Group4ThdVoltageVbH_NoUnit| double     | não         | -       | Grupo 4 Tensão THD Vb_h                   |
|Group4ThdVoltageVcH_NoUnit| double     | não         | -       | Grupo 4 Tensão THD Vc_h                   |
|Group5ThdCurrentIaH_NoUnit| double     | não         | -       | Grupo 5 Corrente THD Ia_h                 |
|Group5ThdCurrentIbH_NoUnit| double     | não         | -       | Grupo 5 Corrente THD Ib_h                 |
|Group5ThdCurrentIcH_NoUnit| double     | não         | -       | Grupo 5 Corrente THD Ic_h                 |
|ActivePowerThd_NoUnit| double     | não         | -       | Grupo 5 Corrente THD Ic_h                 |
|MaximumVoltageA_V| double     | não         | -       | Tensão Máxima Fase A em V                 |
|MaximumVoltageB_V| double     | não         | -       | Tensão Máxima Fase B  em V                |
|MaximumVoltageC_V| double     | não         | -       | Tensão Máxima Fase C  em V                |
|MaximumCurrentA_A| double     | não         | -       | Corrente Máxima Fase A em V               |
|MinimumVoltageA_V| double     | não         | -       | Tensão Minima Fase A em  V                |
|MinimumVoltageB_V| double     | não         | -       | Tensão Máxima Fase B em V                 |
|MinimumVoltageC_V| double     | não         | -       | Tensão Máxima Fase C em V                 |
|TotalEnergyIncremental_Wh| double     | não         | -       | Energia Ativa Direta Incremental em Wh    |
|Q1ReactiveEnergyIncremental_VarH| double     | não         | -       | Energia Reativa Q1 em VarH                |
|Q4ReactiveEnergyIncremental_VarH| double     | não         | -       | Energia Reativa Q4 em  VarH               |
|TotalInductiveReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Indutiva Total em VarH    |
|TotalCapacitiveReactiveEnergy_VarH| double     | não         | -       | Energia Reativa Capacitiva Total em  VarH |
|ReactiveEnergyImportQiiiRi_VarH| double     | não         | -       | Import Energia Ativa QIII (-Ri) em VarH   |
|ReactiveEnergyImportQiiRc_VarH| double     | não         | -       | Import Energia Ativa QI (+Rc) em VarH     |
|MaximumVoltageL1_V| double     | não         | -       | Tensão Máxima L1 em V                     |
|MaximumVoltageL2_V| double     | não         | -       | Tensão Máxima L2 em V                     |
|MaximumVoltageL3_V| double     | não         | -       | Tensão Máxima L3 em V                     |
|MinimumVoltageL1_V| double     | não         | -       | Tensão Minima L1 em  V                    |
|MinimumVoltageL2_V| double     | não         | -       | Tensão Minima L2 em  V                    |
|MinimumVoltageL3_V| double     | não         | -       | Tensão Minima L3  em V                    |
|MaximumCurrentL1_A| double     | não         | -       | Corrente Máxima L1 em A                   |
|MaximumCurrentL2_A| double     | não         | -       | Corrente Máxima L2 em A                   |
|MaximumCurrentL3_A| double     | não         | -       | Corrente Máxima L3 em  A                  |

*String datetime no formato yyyy-MM-ddTHH:mm:ssZ

### Exemplo de Inserção de Memória de Massa

```json
[
  {

    "DateTime": "2022-04-10T22:56:20.716Z",
    "TotalEnergy_Wh": 20
  },
  {
    "DateTime": "2022-04-09T22:56:20.716Z",
    "TotalEnergy_Wh": 10
  }
]
```
### Success Response

**Code** : `200 OK`


## Inserção de Frozen Data

**Sistema** Middleware outros MDCs |
**URL** | `/frozen-data/{serial}`
**Method** | `POST`
**Permissions required** | usuário ADMIN ou INTEGRATION autenticado

Os campos do objeto de Frozen Data são descritos na [tabela - objeto Frozen Data](). O endpoint recebe uma lista desse objeto.

Todos os campos - **exceto _Datetime_ e _TotalEnergy_Wh_** - são nullables.
#### Frozen Data - Descrição de objeto

| nome         | tipo      | obrigatório | tamanho | descrição                        |
|-----|-----------|-------------|---------|----------------------------------|
|  DateTime   | datetime* | sim         | -       | Data e Hora da leitura           |
|  TotalEnergy_Wh   | double    | sim         | 1-3     | Energia Ativa Direta em Wh       |
|  BorderTotalizer_Wh  | double    | não         | -       | Energia Ativa Direta T1 em Wh    |
|ReservedTotalizer_Wh| double     | não         | -       | Energia Ativa Direta T2 em  Wh   |
|OffBorderTotalizer_Wh| double     | não         | -       | Energia Ativa Direta T3 em Wh    |
|TotalIntermediary_Wh| double     | não         | -       | Energia Ativa Direta T4 em  Wh   |
|GeneratedEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa em  Wh     |
|TotalReverseActiveEnergy_Wh| double     | não         | -       | Energia Ativa Reversa Total em Wh |
|GeneratedOffBorderEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T1 em Wh   |
|GeneratedIntermediaryEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T2 em Wh   |
|GeneratedBorderEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T3 em Wh   |
|GeneratedReservedEnergyTotal_Wh| double     | não         | -       | Energia Ativa Reversa T4 em Wh   |
|PositiveReactiveEnergy_Wh| double     | não         | -       | Energia Reativa Positiva em Wh   |
|NegativeReactiveEnergy_Wh| double     | não         | -       | Energia Reativa Negativa |

### Exemplo de Inserção de Frozen Data

```json
[
  {
    "DateTime":"2022-04-10T22:56:20.716Z",
    "TotalEnergy_Wh":20,
    "TotalReverseActiveEnergy_Wh":10
  },
  {
    "DateTime":"2022-04-09T22:56:20.716Z",
    "TotalEnergy_Wh":10,
    "TotalReverseActiveEnergy_Wh":5
  }
]
```
### Success Response

**Code** : `200 OK`




## Confirmação de Faturamento

Em construção
