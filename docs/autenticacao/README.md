﻿# Autenticação

A autenticação no Módulo IoG Services e no Middleware se dá via token JWT. 
A rota que retorna o token é descrita abaixo (processo identico nos 2 módulos):

**URL** | `/users/login/`
**Method** | `GET`
**Permissions required** | None

O usuário deve entrar com email e senha cedidos previamente pelo NEPEN.

```json
{
  "Email": "integracaox@integracaox.com",
  "Password": "integracaoxintegracaox"
}
```

### Success Response

**Code** : `200 OK`

O campo _**accessToken**_ possui o token JWT que deve ser utilizado em todas as requisições ao Sistema IoG.
```json
{
  "authenticated": true,
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyJpbnRlZ3JhY2FveEBpbnRlZ3JhY2FveC5jb20iLCJpbnRlZ3JhY2FveEBpbnRlZ3JhY2FveC5jb20iXSwianRpIjoiM2Q2OGJmY2RlNzE2NGM4NjlhZGExYmE4N2ZkNmIzM2QiLCJjbGllbnRUeXBlIjoiMCIsInJvbGUiOiJBZG1pbiIsIm5iZiI6MTY0OTUzNzM0MiwiZXhwIjoxNjQ5NTQwOTQyLCJpYXQiOjE2NDk1MzczNDIsImlzcyI6IklvRyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NTAwMCJ9.u-FM4P12U1s14DJVwbmm2_l8gt-6Kr-gngO824bXWpo",
  "message": "OK",
  "user": {
    "description": "Integração MDC X",
    "name": "Integração X",
    "email": "integracaox@integracaox.com",
    "password": null,
    "clientType": 0,
    "userType": 4,
    "blocked": false,
    "updatedAt": "2021-04-09T17:49:02Z"
  }
}
```

