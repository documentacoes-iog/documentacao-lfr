const { description } = require('../../package')

const fs = require("fs");
const path = require("path");


module.exports = {
  chainWebpack: (config, isServer) => {
    config.module
      .rule('pdfs')
      .test(/\.pdf$/)
      .use('file-loader')
        .loader('file-loader')
      .options({
        name: `[path][name].[ext]`
      });
    
    config.module.rule('vue')
      .uses.store
      .get('vue-loader').store
      .get('options').transformAssetUrls = {
        video: ['src', 'poster'],
        source: 'src',
        img: 'src',
        image: ['xlink:href', 'href'],
        a: 'href'
      };
  },
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'LFR',
  /* buil folder in gitlab*/
  dest: 'public',
  /** base url in gitlab*/
  base: '/documentacao-integracao-lfr/',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,
  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    logo: '/imagens/nepen_logo.png',  
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    sidebarDepth: 2,
    nav: [
      
    ],
    sidebar: [
      getSideBar("/introducao/", "introducao", "Introdução", true),
      getSideBar("/devices/", "devices", "Dispositivos", true),
      getSideBar("/firmwares/", "firmwares", "Firmwares", true),
      getSideBar("/agendamentos/", "agendamentos", "Agendamentos", true),
      getSideBar("/relatorios/", "relatorios", "Relatorios", true),
    ]
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    'vuepress-plugin-mermaidjs'
  ]
}



function getSideBar(mypath, folder, title, colapse) {
  const extension = [".md"];

  const files = fs
    .readdirSync(path.join(`${__dirname}/../${folder}`))
    .filter(
      item =>
        item.toLowerCase() != "readme.md" &&
        fs.statSync(path.join(`${__dirname}/../${folder}`, item)).isFile() &&
        extension.includes(path.extname(item))
    ). 
    map( item => `${folder}/${item}`);
    // console.log(path.join(`${__dirname}/../${folder}`))
    // console.log(files)
    // console.log("<<<<<<<<")
  return { title: title, path: mypath, collapsable: colapse, children: [ ...files] };
}
