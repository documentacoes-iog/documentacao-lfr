# Documentação Sobre Interface agendamento LFR

A documentação pode ser acessada através do link : [https://integracao-iog.gitlab.io/documentacao-integracao-iog ](https://integracao-iog.gitlab.io/documentacao-integracao-iog )

# Utilização
Clone este repositório e dentro da pasta do projeto utilize o comando:
```bash
npm install
```
isso irá instalar todas as dependências necessárias. Após isso, para escrever a documentação é **necessário** entrar no modo desenvolvimento, para isto basta usar o comando a seguir.

```bash
npm run dev
```
Com isto feito já é possível criar arquivos markdown e editar os existente, estes ficam localizados dentro da pasta docs e são divididos em 4 pastas:
- __introdução__ - Contém os arquivos com fluxos e explicações básicas sobre a integração.
- __crud__ - Contém os arquivos para a documentação do crud de medidores no MDC.
- __notas de serviço (corte e religamento)__ - Contém os arquivos para a documentação sobre as notas de serviço.
- __leituras__ - Contém arquivos sobre as leituras no MDC.
- __leituras de qualidade__ - Contém os arquivos sobre as leituras de qualidade no MDC.

Toda e qualquer alteração feita no repositório master estará automaticamente disponível mo site da documentação, por isso recomenda-se que as modificações sejam feitas em _branchs_ diferentes e após aprovação destas que seja transferida para o master.

